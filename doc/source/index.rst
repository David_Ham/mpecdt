.. Finite element course documentation master file, created by
   sphinx-quickstart on Sat Sep  6 21:48:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


This web page will hold the documentation for the module on the
numerical analysis and implementation of the finite element method
which will be taught to masters students in the Department of
Mathematics at Imperial College London in Spring 2015. Further details
will appear here closer to the start of the module.

.. math::

   \int_\Omega uv\,\mathrm{d}x

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

